FROM python:3

COPY requirements.txt /app/requirements.txt
COPY src/ /app/src/
COPY docker_wsgi.py /app/wsgi.py
COPY gunicorn_config.py /app/gunicorn_config.py

RUN pip install -r /app/requirements.txt

EXPOSE 8900

WORKDIR /app/
CMD gunicorn -c gunicorn_config.py wsgi:application
