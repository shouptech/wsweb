WSWeb
-----

WSWeb is a Python Flask web application intended to read data from Ambient
Weather ObserverIP weather stations.

It has been tested on a WS-1400-IP.

Requirements
============

Python 3+ is required, with the following modules:

* Flask
* Flask-Script
* Flask-SQLAlchemy
* Flask-Cache
* pygal
* Requests
* python3-memcached (If using memcache backend)

Additionally, you'll need something to server WSGI. I use gunicorn, w/ Apache
configured as a reverse proxy.

Installation
============

1. Install Python 3+ and the modules needed above.

2. ``etc/config_sample.py`` contains an example configuration. You'll need to
   copy this to something like ``etc/config.py``  and modify it for your setup.

   - You'll need a database. This can be sqlite, MySQL, Postgres, or anything
     that SQLAlchemy supports.
   - You'll optionally want a caching service. The config file has examples for
     using memcached. Set CACHE_TYPE to 'null' to disable cachine.

3. The ``manage.py`` script is used to manage the application.

   - After configuration, execute
     ``./manage.py -c /path/to/config.py create_tables`` to create the tables
     needed.
   - Execute ``./manage.py -c /path/to/config.py poll`` on a regular basis.
     Put that in a crontab, and execute once a minute. This will regularly add
     data from the ObserverIP module into the database.

4. ``src/wsgi_sample.py`` contains a sample WSGI script. Use this during
   the deployment of the web application.

5. You will need to have your web server serve ``src/wsweb/static`` as
   ``/static``.
