# -*- coding: utf-8 -*-

# !!!! Leaving to True in production is a major security vulnerability.
# Set to False when in production
DEBUG = False

# URL of the weather station's live data page.
# Typically http://<ipaddr>/livedata.htm or http://<fqdn>/livedata.htm
URL = 'http://wx/livedata.htm'

# SQL database URI
# See: <http://docs.sqlalchemy.org/en/latest/core/engines.html>
SQLALCHEMY_DATABASE_URI = 'mysql://wsweb:password@localhost/wsweb'

# Units used in display
# These should be similar to what's configured in your station settings
UNITS = {
    'temp':'°F',
    'wind':'mph',
    'press':'inHg',
    'rain':'in',
    'solar':'W/m²'
}

# Latitude & Longitude of weather station
# Used for forecasts
LATITUDE = 39.75
LONGITUDE = -105.08

# Location to display at top of page
LOCATION = "Edgewater, CO"

# Set to the style used for pygal graphs
# See: <http://www.pygal.org/en/latest/documentation/builtin_styles.html>
from pygal.style import DefaultStyle
STYLE=DefaultStyle

# Cache configuration, used for caching the forecast
# See: <https://pythonhosted.org/Flask-Cache/>
CACHE_TYPE = 'memcached' # Set to 'null' if not using cache
CACHE_DEFAULT_TIMEOUT = 60 # Set to polling interval, caches page views
CACHE_KEY_PREFIX = 'wsweb'
CACHE_MEMCACHED_SERVERS = ['localhost']

# Disables track modifications (not used)
SQLALCHEMY_TRACK_MODIFICATIONS = False
