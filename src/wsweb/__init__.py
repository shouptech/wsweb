# -*- coding: utf-8 -*-
"""
Used to create the app factory
"""

from flask import Flask

app = Flask(__name__)
