# -*- coding: utf-8 -*-
"""
Contains a class useful for scraping data from an Ambient Weather station
live data page.
"""

from html.parser import HTMLParser
import requests

class WSParser(HTMLParser):
    """
    Parses a weather station live data page for data and stores it in a
    dictionary (data).

    Arugments:
    - url: URL of the live data page, e.g., http://1.2.3.4/livedata.htm
    """

    WS_FIELDS = (
        'inTemp',
        'inHumi',
        'AbsPress',
        'RelPress',
        'outTemp',
        'outHumi',
        'windir',
        'avgwind',
        'gustspeed',
        'solarrad',
        'uv',
        'uvi',
        'rainofhourly',
        'rainofdaily',
        'rainofweekly',
        'rainofmonthly',
        'rainofyearly',
        )

    def __init__(self, url):
        HTMLParser.__init__(self)
        self.data = dict(
            zip(WSParser.WS_FIELDS, (None,) * len(WSParser.WS_FIELDS)))
        self.url = url

    def handle_starttag(self, tag, attrs):
        "Parses the page and stores the results in attribute 'data'"
        if tag == 'input':
            dict_attrs = dict(attrs)
            if 'name' in dict_attrs and dict_attrs['name'] in self.data:
                self.data[dict_attrs['name']] = dict_attrs['value']

    def read(self):
        "Retrieves the page and feeds into the parser."
        self.feed(requests.get(self.url).text)
