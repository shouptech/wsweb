# -*- coding: utf-8 -*-
"""
App views
"""

from datetime import datetime, timedelta
from flask import redirect, render_template, url_for, jsonify, request
from wsweb.control import cache, get_forecast, get_last_from_influx
from wsweb import app

def create_app(config=None):
    """ Setup the app object """
    if config is not None:
        app.config.from_pyfile(config)

    cache.init_app(app)

    return app

def cache_key():
    return request.url

def unless_history():
    if 'begin' in request.args.to_dict() and 'end' in request.args.to_dict():
        return False
    else:
        return True

@app.route('/')
def index():
    """ Index page """
    return redirect(url_for('status'))

@app.route('/status')
@cache.cached()
def status():
    """ Status page """
    weather = get_last_from_influx()
    forecast = get_forecast()

    return render_template("status.html", weather=weather, forecast=forecast)

@app.route('/history')
@cache.cached(key_prefix=cache_key, timeout=86400, unless=unless_history)
def history():
    """ History page """
    return render_template("history.html")

@app.route('/ping')
def ping():
    """ Ping point """
    return "PONG"
