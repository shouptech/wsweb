# -*- coding: utf-8 -*-
"""
App control functions
"""

from datetime import datetime
from flask_cache import Cache
from requests import get
from pygal import DateTimeLine
from wsweb.wsparser import WSParser
from wsweb import app
from influxdb import InfluxDBClient
from wsweb.models import WeatherData

cache = Cache()

def get_weather_data():
    # Dictionary used to translate from the weather stations field names to the
    # field names used in the model
    t_fields = {
        'inTemp':'indoor_temp',
        'inHumi':'indoor_humidity',
        'AbsPress':'absolute_pressure',
        'RelPress':'relative_pressure',
        'outTemp':'outdoor_temp',
        'outHumi':'outdoor_humidity',
        'windir':'wind_direction',
        'avgwind':'average_wind_speed',
        'gustspeed':'gust_speed',
        'solarrad':'solar_radiation',
        'uv':'uv',
        'uvi':'uv_index',
        'rainofhourly':'hourly_rain',
        'rainofdaily':'daily_rain',
        'rainofweekly':'weekly_rain',
        'rainofmonthly':'monthly_rain',
        'rainofyearly':'yearly_rain'}

    # Retrieve the data
    parser = WSParser(app.config['URL'])
    parser.read()

    weather_args = {}

    for field, value in parser.data.items():
        weather_args[t_fields[field]] = value

    return WeatherData(weather_args)

def get_last_from_influx():
    """
    Retrieves the latest weather data from the influx DB and returns the data
    as a json dict
    """
    db_client = InfluxDBClient(
        app.config['INFLUX_HOST'],
        app.config['INFLUX_PORT'],
        app.config['INFLUX_USER'],
        app.config['INFLUX_PASS'],
        app.config['INFLUX_DATABASE'])
    result = db_client.query(
        'select last(*) from weather where time > now() - 5m;')
    return result.point_from_cols_vals(
        result.raw['series'][0]['columns'],
        result.raw['series'][0]['values'][0])


def poll_influxdb():
    """
    Polls the current weather and stores it in an influx database
    """
    db_client = InfluxDBClient(
        app.config['INFLUX_HOST'],
        app.config['INFLUX_PORT'],
        app.config['INFLUX_USER'],
        app.config['INFLUX_PASS'],
        app.config['INFLUX_DATABASE'])
    weather_data = get_weather_data()

    json_body = [
        {
            "measurement": "weather",
            "tags": {
                "location": app.config['LOCATION'],
                "station_url": app.config['URL']
            },
            "time": int(datetime.now().timestamp()*1000000),
            "fields": {
                "outdoor_temp": float(weather_data.outdoor_temp),
                "outdoor_humidity": float(weather_data.outdoor_humidity),
                "wind_direction": float(weather_data.wind_direction),
                "average_wind_speed": float(weather_data.average_wind_speed),
                "gust_speed": float(weather_data.gust_speed),
                "solar_radiation": float(weather_data.solar_radiation),
                "uv": float(weather_data.uv),
                "uv_index": float(weather_data.uv_index),
                "hourly_rain": float(weather_data.hourly_rain),
                "daily_rain": float(weather_data.daily_rain),
                "weekly_rain": float(weather_data.weekly_rain),
                "monthly_rain": float(weather_data.monthly_rain),
                "yearly_rain": float(weather_data.yearly_rain)
            }
        }
    ]

    # Check these individually. A 0 value means the indoor sensor was not
    # connected, so we're ignoring the values
    try:
        json_body[0]['fields']['indoor_temp'] = float(weather_data.indoor_temp)
    except ValueError:
        print('Invalid value indoor_temp')
        pass
    try:
        json_body[0]['fields']['indoor_humidity'] = \
            float(weather_data.indoor_humidity)
    except ValueError:
        print('Invalid value indoor_humidity')
        pass
    try:
        json_body[0]['fields']['absolute_pressure'] = \
            float(weather_data.absolute_pressure)
        json_body[0]['fields']['relative_pressure'] = \
            float(weather_data.relative_pressure)
    except ValueError:
        print('Invalid value for pressure')
        pass

    db_client.write_points(json_body, time_precision='u')


@cache.cached(key_prefix='forecast', timeout=3600)
def get_forecast():
    noaa_url = 'http://forecast.weather.gov/MapClick.php'
    params = {'FcstType': 'json',
              'lat': app.config['LATITUDE'],
              'lon': app.config['LONGITUDE']}

    return get(noaa_url, params=params).json()
