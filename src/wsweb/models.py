# -*- coding: utf-8 -*-
"""
Flask-SQLAlchemy models
"""

from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class WeatherData(db.Model):
    """ Data polled from the weather station """
    id = db.Column(db.Integer, primary_key=True)
    polled = db.Column(db.DateTime, default=datetime.now)
    indoor_temp = db.Column(db.Float)
    indoor_humidity = db.Column(db.Integer)
    absolute_pressure = db.Column(db.Float)
    relative_pressure = db.Column(db.Float)
    outdoor_temp = db.Column(db.Float)
    outdoor_humidity = db.Column(db.Integer)
    wind_direction = db.Column(db.Integer)
    average_wind_speed = db.Column(db.Float)
    gust_speed = db.Column(db.Float)
    solar_radiation = db.Column(db.Float)
    uv = db.Column(db.Integer)
    uv_index = db.Column(db.Integer)
    hourly_rain = db.Column(db.Float)
    daily_rain = db.Column(db.Float)
    weekly_rain = db.Column(db.Float)
    monthly_rain = db.Column(db.Float)
    yearly_rain = db.Column(db.Float)

    def __init__(self, weather_data):
        self.indoor_temp = weather_data['indoor_temp']
        self.indoor_humidity = weather_data['indoor_humidity']
        self.absolute_pressure = weather_data['absolute_pressure']
        self.relative_pressure = weather_data['relative_pressure']
        self.outdoor_temp = weather_data['outdoor_temp']
        self.outdoor_humidity = weather_data['outdoor_humidity']
        self.wind_direction = weather_data['wind_direction']
        self.average_wind_speed = weather_data['average_wind_speed']
        self.gust_speed = weather_data['gust_speed']
        self.solar_radiation = weather_data['solar_radiation']
        self.uv = weather_data['uv']
        self.uv_index = weather_data['uv_index']
        self.hourly_rain = weather_data['hourly_rain']
        self.daily_rain = weather_data['daily_rain']
        self.weekly_rain = weather_data['weekly_rain']
        self.monthly_rain = weather_data['monthly_rain']
        self.yearly_rain = weather_data['yearly_rain']
