#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The following is a sample WSGI file. Use this file, in combination with the
Flask documentation to see how to deploy the application.
See: <http://flask.pocoo.org/docs/0.11/deploying/>

You may need to modify the variables below to suit your installation.
This WSGI file assumes you simply cloned the repository into /opt.
"""

# Set this to the directory the application is installed
INSTALLED_LOCATION = "/opt/wsweb/"

# Location of config file. May need to change if this is not where your config
# file is.
CONFIG_FILE = INSTALLED_LOCATION + "etc/config.py"

# Location of the wsweb module. May need to change.
WSWEB_MODULE = INSTALLED_LOCATION + "src"

# -----------------------------------------------------------------------------
# The rest of the code creates the WSGI application. You should not need to
# modify it.
# -----------------------------------------------------------------------------
import sys
sys.path.insert(0, WSWEB_MODULE)

from wsweb.views import create_app
application = create_app(CONFIG_FILE)

if __name__ == "__main__":
    application.run()
