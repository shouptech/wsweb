#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script used to manage aspects of the wsweb application.
"""

from flask_script import Manager, Server
from wsweb.control import poll_influxdb
from wsweb.views import create_app
from wsweb.wsparser import WSParser

manager = Manager(create_app)

@manager.command
def cacti():
    """
    Prints output in format for use with cacti.
    """
    wparser = WSParser(manager.app.config['URL'])
    wparser.read()

    cacti_out = ''

    for key in sorted(wparser.data):
        cacti_out += '%s:%s ' % (key, wparser.data[key])

    print(cacti_out)

@manager.command
def poll_influx():
    """
    Grabs current weather info and stores in influx db
    """
    poll_influxdb()
    print("Added weather data")

def main():
    """
    Main script execution
    """
    server = Server(host='0.0.0.0')
    manager.add_command('runserver', server)
    manager.add_option('-c', '--config', dest='config', required=True)
    manager.run()

if __name__ == "__main__":
    main()
